# Yapily Full Stack Coding Challenge

## Build and run instructions

The prerequisites to build and run this project are Java 10, Maven 3.5.4 and a marvel developer API key pair.

To build the runnable JAR enter the following at the root of the project:

```mvn clean package```

Then to run the built JAR run the following (replacing the public/private keys with your own):

```java -jar -Dmarvel.api.public.key=<public key> -Dmarvel.api.private.key=<private key> target/explorer-1.0-SNAPSHOT```
