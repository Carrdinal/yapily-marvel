package yapily.marvel.service;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import yapily.marvel.model.MarvelCharacter;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import java.util.Collections;
import java.util.List;

@Slf4j
@Service
public class CharacterSearchService {

    private final EntityManager entityManager;

    @Autowired
    public CharacterSearchService(final EntityManagerFactory entityManagerFactory) {
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    @PostConstruct
    public void init() {
        try {
            FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
            fullTextEntityManager.createIndexer().startAndWait();
        } catch (InterruptedException e) {
            log.error("Search service failed to initialise", e);
            throw new RuntimeException(e);
        }
    }

    @Transactional
    public List<MarvelCharacter> fuzzySearch(String searchTerm, int offset, int limit) {

        var fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
        var queryBuilder = fullTextEntityManager.getSearchFactory()
                .buildQueryBuilder()
                .forEntity(MarvelCharacter.class)
                .get();

        var luceneQuery = queryBuilder.keyword()
                .fuzzy()
                .withEditDistanceUpTo(2)
                .withPrefixLength(1)
                .onFields("name", "description")
                .matching(searchTerm)
                .createQuery();

        var jpaQuery = fullTextEntityManager.createFullTextQuery(luceneQuery, MarvelCharacter.class)
                .setFirstResult(offset)
                .setMaxResults(limit);

        List<MarvelCharacter> marvelCharacters;
        try {
            marvelCharacters = jpaQuery.getResultList();
        } catch (NoResultException nre) {
            return Collections.emptyList();
        }

        return marvelCharacters;
    }

    public Integer fuzzySearchCount(String searchTerm) {
        var fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
        var queryBuilder = fullTextEntityManager.getSearchFactory()
                .buildQueryBuilder()
                .forEntity(MarvelCharacter.class)
                .get();

        var luceneQuery = queryBuilder.keyword()
                .fuzzy()
                .withEditDistanceUpTo(2)
                .withPrefixLength(1)
                .onFields("name", "description")
                .matching(searchTerm)
                .createQuery();

        var jpaQuery = fullTextEntityManager.createFullTextQuery(luceneQuery, MarvelCharacter.class)
                .setFirstResult(0)
                .setMaxResults(1);


        return jpaQuery.getMaxResults();
    }
}
