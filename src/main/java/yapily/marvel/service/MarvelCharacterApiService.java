package yapily.marvel.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import yapily.marvel.dto.CharacterDataWrapperDto;
import yapily.marvel.dto.MarvelCharacterDto;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class MarvelCharacterApiService {

    @Value("${marvel.api.public.key}")
    private String marvelPublicKey;

    @Value("${marvel.api.private.key}")
    private String marvelPrivateKey;

    @Value("${marvel.api.host}")
    private String marvelApiHost;

    private final RestTemplate restTemplate;


    public ResponseEntity<CharacterDataWrapperDto> getModifiedCharacterResponseSince(String eTag, Date modifiedSince, int offset, int limit) {
        var headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        if (eTag != null) {
            headers.setIfNoneMatch(eTag);
        }

        var uriComponentsBuilder = getUriComponentsBuilder(marvelApiHost, marvelPrivateKey, marvelPublicKey)
                .path("/v1/public/characters")
                .queryParam("offset", offset)
                .queryParam("limit", limit);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        uriComponentsBuilder.queryParam("modifiedSince", sdf.format(modifiedSince));

        log.debug(uriComponentsBuilder.build().toString());

        return restTemplate.exchange(uriComponentsBuilder.build().toString(), HttpMethod.GET,
                new HttpEntity<>(headers), CharacterDataWrapperDto.class);
    }

    public List<MarvelCharacterDto> getCharacters(int offset, int limit) {
        var wrapper = getCharacterDataWrapper(offset, limit);
        var container = wrapper.getData();
        return container.getResults();
    }

    public Integer getTotalCharacters() {
        var wrapper = getCharacterDataWrapper(0, 1);
        var container = wrapper.getData();
        return container.getTotal();
    }

    private CharacterDataWrapperDto getCharacterDataWrapper(int offset, int limit) {
        var headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        var uriComponentsBuilder = getUriComponentsBuilder(marvelApiHost, marvelPrivateKey, marvelPublicKey)
                .path("/v1/public/characters")
                .queryParam("offset", offset)
                .queryParam("limit", limit);

        log.debug(uriComponentsBuilder.build().toString());

        return restTemplate.exchange(uriComponentsBuilder.build().toString(), HttpMethod.GET,
                new HttpEntity<>(headers), CharacterDataWrapperDto.class).getBody();
    }

    private static UriComponentsBuilder getUriComponentsBuilder(String apiHost, String privateKey, String publicKey) {
        var ts = String.valueOf(System.nanoTime());
        return UriComponentsBuilder.newInstance()
                .scheme("https")
                .host(apiHost)
                .queryParam("ts", ts)
                .queryParam("apikey", publicKey)
                .queryParam("hash", DigestUtils.md5DigestAsHex((ts + privateKey + publicKey).getBytes()));
    }
}
