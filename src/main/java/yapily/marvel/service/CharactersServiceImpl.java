package yapily.marvel.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import yapily.marvel.model.MarvelCharacter;
import yapily.marvel.repository.CharacterRepository;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class CharactersServiceImpl implements CharactersService {

    private final CharacterRepository characterRepository;
    private final CharacterSearchService characterSearchService;

    @Override
    public List<MarvelCharacter> getCharacters(String searchTerm, int offset, int limit) {
        if (StringUtils.isBlank(searchTerm)) {
            return characterRepository.findAll(
                    PageRequest.of(offset / limit, limit, Sort.by(Sort.Order.asc("name")))
            ).getContent();
        }

        return characterSearchService.fuzzySearch(searchTerm, offset, limit);
    }

    @Override
    public Integer getCharactersCount(String searchTerm) {
        if (StringUtils.isBlank(searchTerm)) {
            return Math.toIntExact(characterRepository.findAll(PageRequest.of(0, 1)).getTotalElements());
        }

        return characterSearchService.fuzzySearchCount(searchTerm);
    }

    @Override
    public Optional<MarvelCharacter> getCharacter(Long id) {
        return characterRepository.findById(id);
    }

}
