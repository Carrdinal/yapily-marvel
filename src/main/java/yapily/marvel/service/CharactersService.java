package yapily.marvel.service;

import yapily.marvel.model.MarvelCharacter;

import java.util.List;
import java.util.Optional;

public interface CharactersService {

    List<MarvelCharacter> getCharacters(String searchTerm, int offset, int limit);

    Integer getCharactersCount(String searchTerm);

    Optional<MarvelCharacter> getCharacter(Long id);

}
