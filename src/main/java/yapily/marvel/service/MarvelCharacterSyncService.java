package yapily.marvel.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import yapily.marvel.dto.CharacterDataWrapperDto;
import yapily.marvel.dto.EventSummaryDto;
import yapily.marvel.dto.MarvelCharacterDto;
import yapily.marvel.dto.SeriesSummaryDto;
import yapily.marvel.dto.StorySummaryDto;
import yapily.marvel.model.CharacterSyncETag;
import yapily.marvel.model.MarvelCharacter;
import yapily.marvel.repository.CharacterRepository;
import yapily.marvel.repository.CharacterSyncETagRepository;

import javax.annotation.PostConstruct;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
@Service
@RequiredArgsConstructor
public class MarvelCharacterSyncService {

    private final CharacterRepository characterRepository;
    private final CharacterSyncETagRepository characterSyncETagRepository;
    private final MarvelCharacterApiService marvelApiService;

    @PostConstruct
    public void init() {
        if (characterSyncETagRepository.findAll().size() != 0) {
            updateCharacters();
            return;
        }

        // Scrape for the first time.
        int callsToMake = marvelApiService.getTotalCharacters() / 100;
        log.info(String.format("Scraping Marvel API, %d calls to make", callsToMake + 1));

        IntStream.range(0, callsToMake)
                .parallel()
                .forEach(it -> characterRepository.saveAll(
                        marvelApiService.getCharacters(it * 100, 100).stream()
                                .map(mapCharacterDtoToModel())
                                .collect(Collectors.toList())
                        )
                );

        updateLatestETag();

        log.info("Scraping complete.");
    }

    @Scheduled(fixedDelay = 300000, initialDelay = 30000) // Every 5 minutes
    private void updateCharacters() {
        log.info("Polling Marvel API for updates to characters...");
        try {
            var latestSyncETag = characterSyncETagRepository.findAll().stream()
                    .max(Comparator.comparing(CharacterSyncETag::getLastCalledAt))
                    .orElseThrow(() -> new RuntimeException("Unable to find latest sync ETag."));

            var response = marvelApiService.getModifiedCharacterResponseSince(
                    latestSyncETag.getETag(), latestSyncETag.getLastCalledAt(), 0, 1);

            if (response.getStatusCode() == HttpStatus.NOT_MODIFIED) {
                return;
            }

            CharacterDataWrapperDto wrapper = response.getBody();

            log.info(String.format("Found %d modified characters. Updating servers cache.", wrapper.getData().getTotal()));
            IntStream.range(0, wrapper.getData().getTotal())
                    .parallel()
                    .forEach(it -> characterRepository.saveAll(
                            getModifiedCharactersSince(latestSyncETag, it))
                    );

            updateLatestETag();
        } catch (Exception e) {
            log.error("Failed to update characters from marvel API", e);
        }
    }

    private void updateLatestETag() {
        var lastCalledAt = new Date();
        var eTag = marvelApiService.getModifiedCharacterResponseSince(null, lastCalledAt, 0, 1)
                .getBody()
                .getEtag();

        characterSyncETagRepository.save(CharacterSyncETag.builder()
                .lastCalledAt(new Date())
                .eTag(eTag)
                .build()
        );
    }

    private List<MarvelCharacter> getModifiedCharactersSince(CharacterSyncETag latestSyncETag, int page) {
        return marvelApiService.getModifiedCharacterResponseSince(null, latestSyncETag.getLastCalledAt(), page * 100, 100)
                .getBody().getData().getResults().stream()
                .map(mapCharacterDtoToModel())
                .collect(Collectors.toList());
    }

    private Function<MarvelCharacterDto, MarvelCharacter> mapCharacterDtoToModel() {
        return dto -> MarvelCharacter.builder()
                .id(dto.getId())
                .name(dto.getName())
                .description(dto.getDescription())
                .thumbnailUrl(dto.getThumbnail().getUrl())
                .events(dto.getEvents().getItems().stream()
                        .map(EventSummaryDto::getName)
                        .collect(Collectors.toList()))
                .stories(dto.getStories().getItems().stream()
                        .map(StorySummaryDto::getName)
                        .collect(Collectors.toList()))
                .series(dto.getSeries().getItems().stream()
                        .map(SeriesSummaryDto::getName)
                        .collect(Collectors.toList()))
                .build();
    }

}
