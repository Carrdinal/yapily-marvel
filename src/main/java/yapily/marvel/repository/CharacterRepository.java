package yapily.marvel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import yapily.marvel.model.MarvelCharacter;

@Repository
public interface CharacterRepository extends JpaRepository<MarvelCharacter, Long> {

}
