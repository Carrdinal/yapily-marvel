package yapily.marvel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import yapily.marvel.model.CharacterSyncETag;

import java.util.List;

@Repository
public interface CharacterSyncETagRepository extends JpaRepository<CharacterSyncETag, Long> {

}
