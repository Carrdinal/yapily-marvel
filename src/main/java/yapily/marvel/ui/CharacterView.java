package yapily.marvel.ui;

import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.ListItem;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.html.UnorderedList;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import yapily.marvel.service.CharactersService;

import java.util.List;

@Route("character")
@StyleSheet("frontend://styles/shared-styles.css")
public class CharacterView extends VerticalLayout implements HasUrlParameter<Long> {

    private CharactersService charactersService;
    private H1 title;
    private Paragraph description;
    private Image thumbnail;
    private FlexLayout detailsContainer;
    private Div storiesComponent;
    private Div eventsComponent;
    private Div seriesComponent;

    public CharacterView(@Autowired @Qualifier("charactersServiceImpl") CharactersService charactersService) {
        this.charactersService = charactersService;

        title = new H1();
        description = new Paragraph();

        thumbnail = new Image();
        thumbnail.addClassNames("thumbnail");
        var thumbnailContainer = new Div(thumbnail);
        thumbnailContainer.addClassNames("thumbnail-container");

        storiesComponent = new Div();
        eventsComponent = new Div();
        seriesComponent = new Div();

        detailsContainer = new FlexLayout(storiesComponent, seriesComponent, eventsComponent);
        detailsContainer.addClassNames("details-container");


        add(thumbnailContainer, title, description, detailsContainer);
        addClassNames("content");
    }


    @Override
    public void setParameter(BeforeEvent event, Long parameter) {
        charactersService.getCharacter(parameter)
                .ifPresent(c -> {
                    this.thumbnail.setSrc(c.getThumbnailUrl());

                    title.add(c.getName());
                    description.add(c.getDescription());

                    if (c.getStories().size() > 0) {
                        buildStoriesComponent(c.getStories());
                    } else {
                        detailsContainer.remove(storiesComponent);
                    }

                    if (c.getEvents().size() > 0) {
                        buildEventsComponent(c.getEvents());
                    } else {
                        detailsContainer.remove(eventsComponent);
                    }

                    if (c.getSeries().size() > 0) {
                        buildSeriesComponent(c.getSeries());
                    } else {
                        detailsContainer.remove(seriesComponent);
                    }
                });
    }

    private void buildStoriesComponent(List<String> stories) {
        var title = new H2("Stories");
        var list = new UnorderedList();

        stories.stream().map(ListItem::new).forEach(list::add);

        storiesComponent.addClassName("details-item");
        storiesComponent.add(title, list);
    }

    private void buildEventsComponent(List<String> events) {
        var title = new H2("Events");
        var list = new UnorderedList();

        events.stream().map(ListItem::new).forEach(list::add);

        eventsComponent.addClassName("details-item");
        eventsComponent.add(title, list);
    }

    private void buildSeriesComponent(List<String> stringList) {
        var title = new H2("Series");
        var list = new UnorderedList();

        stringList.stream().map(ListItem::new).forEach(list::add);

        seriesComponent.addClassName("details-item");
        seriesComponent.add(title, list);
    }

}
