package yapily.marvel.ui;

import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.vaadin.klaudeta.PaginatedGrid;
import yapily.marvel.service.CharactersService;
import yapily.marvel.model.MarvelCharacter;

@Route
@StyleSheet("frontend://styles/shared-styles.css")
@Slf4j
@SuppressWarnings("unused")
public class MainView extends VerticalLayout {

    private static final int PAGE_SIZE = 10;

    private PaginatedGrid<MarvelCharacter> grid;

    private CharactersService charactersService;

    public MainView(@Autowired @Qualifier("charactersServiceImpl") CharactersService charactersService) {
        this.charactersService = charactersService;

        var search = new TextField(null, "search here");
        search.setValueChangeMode(ValueChangeMode.EAGER);

        var dataProvider = getDataProvider();
        var wrapper = dataProvider.withConfigurableFilter();

        grid = buildCharacterPaginatedGrid(wrapper);

        search.addValueChangeListener(e -> {
            log.debug(e.getValue());
            wrapper.setFilter(e.getValue());

            // This calls the private refreshPaginator() method in PaginatedGrid, required to re-render the component.
            grid.setDataProvider(wrapper);
        });

        add(search, grid);
        addClassNames("content");
    }

    private DataProvider<MarvelCharacter, String> getDataProvider() {
        return DataProvider.fromFilteringCallbacks(
                query -> {
                    int offset = query.getOffset();
                    int limit = query.getLimit();

                    var characters = charactersService.getCharacters(query.getFilter().orElse(""), offset, limit);

                    return characters.stream();
                },
                query -> charactersService.getCharactersCount(query.getFilter().orElse(""))
        );
    }

    private PaginatedGrid<MarvelCharacter> buildCharacterPaginatedGrid(ConfigurableFilterDataProvider<MarvelCharacter, Void, String> wrapper) {
        var grid = new PaginatedGrid<MarvelCharacter>();

        grid.addColumn(new ComponentRenderer<>(this::buildCharacterComponent));

        grid.setPageSize(PAGE_SIZE);
        grid.setPaginatorSize(5);

        grid.setDataProvider(wrapper);
        return grid;
    }

    private Div buildCharacterComponent(MarvelCharacter character) {
        var div = new Div();
        div.addClassNames("character", "list-item");

        var button = new RouterLink(character.getName(),
                CharacterView.class,
                character.getId());

        div.add(button);
        return div;
    }

}
