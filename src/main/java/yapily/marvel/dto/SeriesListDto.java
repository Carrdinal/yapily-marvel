package yapily.marvel.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SeriesListDto {

    private Integer available;

    private Integer returned;

    private String collectionURI;

    private List<SeriesSummaryDto> items;

}
