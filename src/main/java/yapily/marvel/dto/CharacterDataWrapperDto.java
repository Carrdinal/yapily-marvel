package yapily.marvel.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CharacterDataWrapperDto {

    private Integer code;

    private String status;

    private String attributionText;

    private String attributionHTML;

    private CharacterDataContainerDto data;

    private String etag;

}
