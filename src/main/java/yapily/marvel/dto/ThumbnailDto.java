package yapily.marvel.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ThumbnailDto {

    private String path;
    private String extension;

    public String getUrl() {
        return path + "." + extension;
    }
}
