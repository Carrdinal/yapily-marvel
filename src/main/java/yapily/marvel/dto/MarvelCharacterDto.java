package yapily.marvel.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.TermVector;

import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MarvelCharacterDto {

    private Long id;

    private String name;

    private String description;

    private ThumbnailDto thumbnail;

    private StoryListDto stories;

    private SeriesListDto series;

    private EventListDto events;

}
