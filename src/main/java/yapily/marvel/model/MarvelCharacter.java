package yapily.marvel.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.KeywordAnalyzer;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TermVector;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Indexed
@Entity
@Table(name = "marvel_character")
public class MarvelCharacter {

    @Id
    private Long id;

    @Field(termVector = TermVector.YES)
    private String name;

    @Field(termVector = TermVector.YES)
    @Column(columnDefinition = "LONGTEXT")
    private String description;

    @Column(name = "thumbnail_url")
    private String thumbnailUrl;

    @ElementCollection(fetch = FetchType.EAGER)
    @Column(columnDefinition = "LONGTEXT")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<String> stories;

    @ElementCollection(fetch = FetchType.EAGER)
    @Column(columnDefinition = "LONGTEXT")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<String> series;

    @ElementCollection(fetch = FetchType.EAGER)
    @Column(columnDefinition = "LONGTEXT")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<String> events;

}
