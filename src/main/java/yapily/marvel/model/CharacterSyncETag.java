package yapily.marvel.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "character_sync_etag")
public class CharacterSyncETag {

    @Id
    @Column(name = "last_called_at")
    private Date lastCalledAt;

    @Column(name = "etag")
    private String eTag;

}
